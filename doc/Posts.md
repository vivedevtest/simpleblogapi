# API Posts

## FindAll

### URL :

```http
GET /api/posts
```
> *\*\* **Require : `Authorization` header***

### Response :

```javascript
[
    {
        "_id": "5da04e83f2911d5c2891b64a",
        "title": "TT1",
        "description": "TEST",
        "author": "5da04e74f2911d5c2891b649",
        "createdAt": "2019-10-11T09:42:27.676Z",
        "updatedAt": "2019-10-11T09:42:27.676Z"
    },
    {
        "_id": "5da04e86f2911d5c2891b64b",
        "title": "TT2",
        "description": "TEST",
        "author": "5da04e74f2911d5c2891b649",
        "createdAt": "2019-10-11T09:42:30.341Z",
        "updatedAt": "2019-10-11T09:42:30.341Z"
    },
    {
        "_id": "5da04e88f2911d5c2891b64c",
        "title": "TT3",
        "description": "TEST",
        "author": "5da04e74f2911d5c2891b649",
        "createdAt": "2019-10-11T09:42:32.898Z",
        "updatedAt": "2019-10-11T09:42:32.898Z"
    }
]
```
---
---
## Create New Post

### URL :

```http
POST /api/posts
```
> *\*\* **Require : `Authorization` header***

### Request :
| Parameter | Type     | Description                        |
| :-------- | :------- | :--------------------------------- |
| `title` | `string` | **Required** |
| `description` | `string` | **Required** |
| `authorId` | `string` | **Required** |
 
### Response :

```javascript
{
    "_id": "5da358d5e45f45471cfcfb6a",
    "title": "TESTPOST1",
    "description": "TEST111",
    "author": "5da35496931c6e3fe47e89b3",
    "createdAt": "2019-10-13T17:03:17.191Z",
    "updatedAt": "2019-10-13T17:03:17.191Z"
}
```
---
---
## Find by TitleName

### URL :

```http
GET /api/posts:title
```
> *\*\* **Require : `Authorization` header***

### Response :

```javascript
{
    "_id": "5da04e86f2911d5c2891b64b",
    "title": "TT2",
    "description": "TEST",
    "author": {
        "_id": "5da04e74f2911d5c2891b649",
        "name": "BBB",
        "createdAt": "2019-10-11T09:42:12.957Z",
        "updatedAt": "2019-10-11T09:42:12.957Z"
    },
    "createdAt": "2019-10-11T09:42:30.341Z",
    "updatedAt": "2019-10-11T09:42:30.341Z"
}
```
---
---
## Find by AuthorId

### URL :

```http
GET /api/posts/author/:authorId
```
> *\*\* **Require : `Authorization` header***

### Response :

```javascript
[
    {
        "_id": "5da04e83f2911d5c2891b64a",
        "title": "TT1",
        "description": "TEST",
        "author": "5da04e74f2911d5c2891b649",
        "createdAt": "2019-10-11T09:42:27.676Z",
        "updatedAt": "2019-10-11T09:42:27.676Z"
    },
    {
        "_id": "5da04e86f2911d5c2891b64b",
        "title": "TT2",
        "description": "TEST",
        "author": "5da04e74f2911d5c2891b649",
        "createdAt": "2019-10-11T09:42:30.341Z",
        "updatedAt": "2019-10-11T09:42:30.341Z"
    },
    {
        "_id": "5da04e88f2911d5c2891b64c",
        "title": "TT3",
        "description": "TEST",
        "author": "5da04e74f2911d5c2891b649",
        "createdAt": "2019-10-11T09:42:32.898Z",
        "updatedAt": "2019-10-11T09:42:32.898Z"
    }
]
```
---
---
## Update Post

### URL :

```http
PUT /api/posts/:titleId
```
> *\*\* **Require : `Authorization` header***

### Request : 

| Parameter | Type     | Description                        |
| :-------- | :------- | :--------------------------------- |
| `title` | `string` | **Required** |
| `description` | `string` |  |

### Response :

```javascript
{
    "_id": "5da04e88f2911d5c2891b64c",
    "title": "TESTNAJA",
    "description": "TESTJINGJING",
    "author": "5da04e74f2911d5c2891b649",
    "createdAt": "2019-10-11T09:42:32.898Z",
    "updatedAt": "2019-10-13T17:21:45.366Z"
}
```
---
---
## Delete Post

### URL :

```http
DELETE /api/posts/:titleId
```
> *\*\* **Require : `Authorization` header***

### Response :

```javascript
{
    "message": "Post deleted successfully!"
}
```
---
---


