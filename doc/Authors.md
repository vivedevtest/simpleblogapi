# API Authors

## FindAll

### URL :

```http
GET /api/authors
```
> *\*\* **Require : `Authorization` header***

### Response :

```javascript
[
    {
        "_id": "5da04e1df2911d5c2891b647",
        "name": "AAA",
        "createdAt": "2019-10-11T09:40:45.326Z",
        "updatedAt": "2019-10-11T09:40:45.326Z"
    },
    {
        "_id": "5da04e74f2911d5c2891b649",
        "name": "BBB",
        "createdAt": "2019-10-11T09:42:12.957Z",
        "updatedAt": "2019-10-11T09:42:12.957Z"
    }
]
```
---
---
## Create New Author

### URL :

```http
POST /api/authors
```
> *\*\* **Require : `Authorization` header***

### Request :
| Parameter | Type     | Description                        |
| :-------- | :------- | :--------------------------------- |
| `name` | `string` | **Required** |
 
### Response :

```javascript
{
    "_id": "5da35496931c6e3fe47e89b3",
    "name": "CCC",
    "createdAt": "2019-10-13T16:45:10.437Z",
    "updatedAt": "2019-10-13T16:45:10.437Z"
}
```
---
---
## Find by AuthorName

### URL :

```http
GET /api/authors/:author
```
> *\*\* **Require : `Authorization` header***

### Response :

```javascript
{
    "_id": "5da04e1df2911d5c2891b647",
    "name": "AAA",
    "createdAt": "2019-10-11T09:40:45.326Z",
    "updatedAt": "2019-10-11T09:40:45.326Z"
}
```
---
---
## Update Author

### URL :

```http
PUT /api/authors/:authorId
```
> *\*\* **Require : `Authorization` header***

### Request : 

| Parameter | Type     | Description                        |
| :-------- | :------- | :--------------------------------- |
| `name` | `string` | **Required** |

### Response :

```javascript
{
    "_id": "5da04e1df2911d5c2891b647",
    "name": "ASDF",
    "createdAt": "2019-10-11T09:40:45.326Z",
    "updatedAt": "2019-10-13T16:54:49.756Z"
}
```
---
---
## Delete Author

### URL :

```http
DELETE /api/authors/:authorId
```
> *\*\* **Require : `Authorization` header***

### Response :

```javascript
{
    "message": "Author deleted successfully!"
}
```
---
---


