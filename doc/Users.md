# API Users

## FindAll

### URL :

```http
GET /api/users
```

### Response :

```javascript
[
    {
        "username": "test",
        "createdAt": "2019-10-11T15:03:38.388Z"
    },
    {
        "username": "admin",
        "createdAt": "2019-10-12T10:04:34.522Z"
    }
]
```
---
---
## Create New User

### URL :

```http
POST /api/users
```

### Request :
| Parameter | Type     | Description                        |
| :-------- | :------- | :--------------------------------- |
| `username` | `string` | **Required** |
| `pasword` | `string` | **Required (MinLenght : 4)** |
 
### Response :

```javascript
{
    "bodyUser": {
        "_id": "5da32ff8b5d1a05304903b36",
        "username": "test2",
        "password": "$2a$08$XBA8vop/CXDowJHQBFDtaeGf6LtLxZLTcUEjhwQuofM0tZhELLOMe",
        "tokens": [
            {
                "_id": "5da32ff9b5d1a05304903b37",
                "token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEzMmZmOGI1ZDFhMDUzMDQ5MDNiMzYiLCJpYXQiOjE1NzA5NzU3MzcsImV4cCI6MTU3MDk3NjAzN30.V06ZBP_QQsAXGzmbk8e3i2c5caUvBYV7FvNKuilLob_0EUtvtExIXGQAaZUAJhvD-5LCnwIF3B-kL6iLGJ1U3g"
            }
        ],
        "createdAt": "2019-10-13T14:08:56.983Z",
        "updatedAt": "2019-10-13T14:08:57.065Z"
    },
    "token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEzMmZmOGI1ZDFhMDUzMDQ5MDNiMzYiLCJpYXQiOjE1NzA5NzU3MzcsImV4cCI6MTU3MDk3NjAzN30.V06ZBP_QQsAXGzmbk8e3i2c5caUvBYV7FvNKuilLob_0EUtvtExIXGQAaZUAJhvD-5LCnwIF3B-kL6iLGJ1U3g",
    "refreshToken": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEzMmZmOGI1ZDFhMDUzMDQ5MDNiMzYiLCJpYXQiOjE1NzA5NzU3MzcsImV4cCI6MTU3MDk3OTMzN30.iPRECsLUGt4cyznBFab0KvNuryeXDRY6WVteKNtCaegVHNB_phzzFZn0lWZIEDA3Or_SN8E9HXafQBJ79X0uEQ"
}
```
---
---
## Login

### URL :

```http
POST /api/users/login
```

### Request :
| Parameter | Type     | Description                        |
| :-------- | :------- | :--------------------------------- |
| `username` | `string` | **Required** |
| `pasword` | `string` | **Required** |

### Response :

```javascript
 {
    "user": {
        "_id": "5da099cad8c0ae5d247d3800",
        "username": "test",
        "password": "$2a$08$AGi471nYAcXNdMLSgDb3VO8tIqeygSY.FN4XgNWlIqhTuVrAI2Opa",
        "tokens": [
            {
                "_id": "5da3361db5d1a05304903b38",
                "token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEwOTljYWQ4YzBhZTVkMjQ3ZDM4MDAiLCJpYXQiOjE1NzA5NzczMDksImV4cCI6MTU3MDk3NzYwOX0.IhEC15RyEHYgyRv6U-eDMOU5D5ooJoYheUFdhwbUb0QQI_PLdV4i323TXCErQBnvrIHugRMjRNRvhBw7jEwmmg"
            }
        ],
        "createdAt": "2019-10-11T15:03:38.388Z",
        "updatedAt": "2019-10-13T14:35:09.719Z"
    },
    "token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEwOTljYWQ4YzBhZTVkMjQ3ZDM4MDAiLCJpYXQiOjE1NzA5NzczMDksImV4cCI6MTU3MDk3NzYwOX0.IhEC15RyEHYgyRv6U-eDMOU5D5ooJoYheUFdhwbUb0QQI_PLdV4i323TXCErQBnvrIHugRMjRNRvhBw7jEwmmg",
    "refreshToken": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEwOTljYWQ4YzBhZTVkMjQ3ZDM4MDAiLCJpYXQiOjE1NzA5NzczMDksImV4cCI6MTU3MDk4MDkwOX0.xbsJxfW06IGp1XjQ2GrN1oAddH9GRkIoFyUfTXkkof8C8pK7LNXGUmxCTN0EL2hXFS4dqzsDHVcl7Ttaqww2Hg"
}   
```
---
---
## Delete User

### URL :

```http
DELETE /api/users
```
> *\*\* **Require : `Authorization` header and `Admin` user.***

### Response :

```javascript
{
    "message": "User deleted successfully!"
}
```
---
---
## Refresh Token

### URL :

```http
POST /api/users/refreshToken
```
### Request : 

| Parameter | Type     | Description                        |
| :-------- | :------- | :--------------------------------- |
| `username` | `string` | **Required** |
| `refreshToken` | `string` | **Required** |

### Response :

```javascript
{
    "token": "eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZGEzNGVhZDcxMjliNzU4YzgxZDA3YTMiLCJpYXQiOjE1NzA5ODQ0NjYsImV4cCI6MTU3MDk4NDc2Nn0.TvrGkH9Zq_sTNoM14mRU2L4_UQL4b2d_td3o3hzATromVrcR5UCxP6VLNAZFStVNKN21VMu0WMzTurVQtqhMMw"
}
```
---
---


