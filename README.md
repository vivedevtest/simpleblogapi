# SimpleBlogAPI

## Authorization

All API requests require the use of a generated API key.

To authenticate an API request, you should provide your API key in the `Authorization` header.

> `Authorization` : Bearer [Token]

* ***Token expired 5 min***

* ***RefreshToken expired 60 min***

## Status Codes

Returns the following status codes in its API:

| Status Code | Description             |
| :---------- | :---------------------- |
| 200         | `OK`                    |
| 201         | `CREATED`               |
| 400         | `BAD REQUEST`           |
| 403         | `FORBIDDEN`             |
| 404         | `NOT FOUND`             |
| 500         | `INTERNAL SERVER ERROR` |
