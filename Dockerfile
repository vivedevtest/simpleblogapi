FROM node:10.16.3-alpine
LABEL maintainer "Werapat Thanakijwarakool"
RUN mkdir -p /app/simpleblogapi

WORKDIR /app/simpleblogapi
#USER node

COPY . /app/simpleblogapi

#RUN apt-get update rm -rf /var/lib/apt/lists/*
RUN npm install

EXPOSE 8081
CMD ["npm","start"]