const User = require("../models/user.model");
const List = {};

//Creat new user
exports.create = async (req, res) => {
  try {
    const bodyUser = new User(req.body);

    await bodyUser.save();
    const { token, refreshToken } = await bodyUser.generateAuthToken();
    List[refreshToken] = refreshToken;
    res.status(201).json({ bodyUser, token, refreshToken });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

//Find All User
exports.findAll = async (req, res) => {
  try {
    let resultAll = await User.aggregate([
      { $project: { _id: 0, username: 1, createdAt: 1 } }
    ]);

    if (resultAll === null) {
      return res.status(404).json({ message: "User not found" });
    }
    res.json(resultAll);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};

//Login
exports.login = async (req, res) => {
  try {
    const { username, password } = req.body;
    let user = await User.findByCredentials(username, password);
    if (!user) {
      return res
        .status(401)
        .json({ message: "Login failed! Check authentication credentials" });
    }
    const { token, refreshToken } = await user.generateAuthToken();
    List[username] = username;
    List[refreshToken] = refreshToken;
    res.json({ user, token, refreshToken });
  } catch (err) {
    res.status(400).json({ message: err.message });
  }
};

//Find by Username And Delete
exports.findByUsernameAndDelete = async (req, res) => {
  try {
    if (req.user.username != "admin") {
      return res.status(400).json({ message: "Method require admin only" });
    }

    const { username } = req.body;

    if (username === "admin" && username in List) {
      return res.status(400).json({ message: "Could not delete admin user" });
    }

    const deleteUser = await User.findOneAndRemove({ username });
    if (deleteUser === null) {
      return res.status(404).json({
        message: "User not found with username " + username
      });
    }
    res.json({ message: "User deleted successfully!" });
  } catch (err) {
    if (err.kind === "ObjectId" || err.name === "NotFound") {
      return res.status(404).json({
        message: "User not found with username " + username
      });
    }
    res.status(500).json({
      message: "Could not delete user with username " + username
    });
  }
};

//Refresh Token
exports.refreshToken = async (req, res) => {
  try {
    const { username, refreshToken } = req.body;

    let user = await User.findOne({ username });
    if (!user) {
      return res.status(404).json({ message: "Invalid request" });
    }

    if (refreshToken && refreshToken in List) {
      const token = await User.generateAuthTokenforRefresh(user, refreshToken);
      res.json({ token: token });
    } else {
      res.status(404).json({ message: "Invalid request" });
    }
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
};
