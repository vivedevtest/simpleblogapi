const Author = require('../models/author.model');
const Post = require('../models/post.model');

//Save
exports.create = async(req,res) => {
  try{
      const bodyPost = new Post({
        title: req.body.title,
        description: req.body.description,
        author : req.body.authorId
      });

      let author = await Author.findById(req.body.authorId);
      if (author === null) { return res.status(400).json({ message:'Author Id not found' })};

      let newPost = await bodyPost.save();
      res.status(201).json(newPost);

  } catch(err) {
    res.status(500).json({ message: err.message });
  }
};

//Find All Post
exports.findAll = async (req, res) => {
  try{
    let resultAll = await Post.find();
    res.send(resultAll);
  } catch(err){
    res.status(500).send( { message: err.message });
  }  
};

// Find a Post by Name
exports.findByTitleName = async (req, res) => {
  try{
  let resultTitle = await Post.findOne({ title: req.params.title }).populate('author');
  
  if (resultTitle === null){ return res.status(404).json({ message: "Post not found with given name " + req.params.title })};
  res.json(resultTitle);
  } catch(err){
    if(err.kind === 'ObjectId') { return res.status(404).json({ message: "Post not found with given name " + req.params.title })};
    res.status(500).json({ message: "Error retrieving Post with given Author " + req.params.title }); 
  }
};

// Find all posts by a Author
exports.findByAuthorId = async (req, res) => {
  try{
    let resultTitles = await Post.find({ author : req.params.authorId });
    
    if (resultTitles.length === 0) { return res.status(404).json({ message: "Posts not found with given Author Id " + req.params.authorId })}; 
    res.json(resultTitles);
  } catch(err){
    if (err.kind === 'ObjectId') { return res.status(404).json({ message: "Posts not found with given Author Id " + req.params.authorId })}; 
    res.status(500).json({ message: "Error retrieving Posts with given Author Id " + req.params.authorId });
  }
};

//Find by TitleId And Update
exports.findByTitleIdAndUpdate = async (req,res) =>{
  try{
    let updateTitle = await Post.findByIdAndUpdate(req.params.titleId,{$set:req.body},{new:true});

    if(updateTitle === null) { return res.status(404).json({ message: "Post not found with id " + req.params.titleId })};
    res.json(updateTitle);
  } catch(err){
    if(err.kind === 'ObjectId') { return res.status(404).json({ message: "Post not found with id " + req.params.titleId })};
    res.status(500).json({ message: "Error updating post with id " + req.params.titleId });
  }
};

//Find by TitleId And Delete
exports.findByTitleIdAndDelete = async (req, res) => {
  try{
    let deleteTitle = await Post.findByIdAndRemove(req.params.titleId);

    if(deleteTitle === null) { return res.status(404).json({ message: "Post not found with id " + req.params.titleId })};
    res.json({message: "Post deleted successfully!"});
  } catch(err){
    if(err.kind === 'ObjectId' || err.name === 'NotFound') {  return res.status(404).json({ message: "Post not found with id " + req.params.titleId})};
    res.status(500).json({ message: "Could not delete post with id " + req.params.titleId });
  }
};



