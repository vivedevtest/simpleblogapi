const Author = require('../models/author.model');
const Post = require('../models/post.model');

//Save
exports.create = async(req,res) => {
    try{
        const bodyAuthor = new Author({
            name: req.body.name
        });
  
        let newAuthor = await bodyAuthor.save();
        res.status(201).json(newAuthor);
    } catch(err){
        res.status(500).json({ message: err.message });
    }
};

//Find All Author
exports.findAll = async(req, res) => {
    try{
        let resultAll = await Author.find();

        if (resultAll === null) { return res.status(404).json({ message: "Author not found" })}; 
        res.json(resultAll);
    } catch(err){
        res.status(500).json({ message: err.message });
    }
};

//Find a Author by Name
exports.findByAuthorName = async(req, res) => {
    try{
        let resultAuthor = await Author.findOne({ name: req.params.author });

        if (resultAuthor === null){ return res.status(404).json({ message: "Author not found with given name " + req.params.author })};
    res.json(resultAuthor); 
    } catch(err){
        res.status(500).json({ message: err.message });
    }  
};

//Find by AuthorId And Update
exports.findByAuthorIdAndUpdate = async(req,res) =>{
    try{
        let updateAuthor = await Author.findByIdAndUpdate(req.params.authorId,{ name:req.body.name},{new:true});

        if(updateAuthor === null) { return res.status(404).json({ message: "Author not found with id " + req.params.authorId })};
        res.json(updateAuthor);
    } catch(err){
        if(err.kind === 'ObjectId') { return res.status(404).json({ message: "Author not found with id " + req.params.authorId })};
        res.status(500).json({ message: "Error updating author with id " + req.params.authorId });
    }
};

//Find by AuthorId And Delete
exports.findByAuthorIdAndDelete = async(req, res) => {
    try{
        let deleteAuthor = await Author.findByIdAndRemove(req.params.authorId);

        if(deleteAuthor === null) { return res.status(404).json({ message: "Author not found with id " + req.params.authorId })};
        let deleteTitleByAuthor = await Post.deleteMany({ author : req.params.authorId });
        if (deleteTitleByAuthor.deletedCount === 0){return res.json({message: "Author deleted successfully!"})};
        res.json({message: "Author deleted successfully! and Post deleted count " + deleteTitleByAuthor.deletedCount});
    } catch(err){
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {return res.status(404).json({ message: "Author not found with id " + req.params.authorId })};
        res.status(500).json({ message: "Could not delete author with id " + req.params.authorId });
    }
};
