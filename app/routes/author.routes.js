var express = require('express');
var router = express.Router() ;
var Author = require('../controller/author.controller');

//Find All
router.get('/', Author.findAll);
 
//Find a single Author by Name
router.get('/:author', Author.findByAuthorName);

//Create
router.post('/', Author.create);

//Update
router.put('/:authorId', Author.findByAuthorIdAndUpdate);

//Delete
router.delete('/:authorId', Author.findByAuthorIdAndDelete);

module.exports = router


///////////////////////////////////////////////////////////////////////////////


/* module.exports = function(app) {
 
    var Author = require('../controller/author.controller');
    
    //Find 
    app.get('/api/author', Author.findAll);

    //Find a single Author by Name
    //app.get('/api/author/:author', Author.findByAuthorName);

    //Save
    //app.post('/api/author', Author.save);

    //Update


    //Delete

    
  }; */