var express = require("express");
var router = express.Router();
const authors = require("../routes/author.routes");
const posts = require("../routes/post.routes");
const users = require("../routes/user.routes");
const auth = require("../middleware/auth");

router.use("/authors", auth, authors);
router.use("/posts", auth, posts);
router.use("/users", users);

router.all("/", (req, res) =>
  res.json({
    Message: "API Home",
    Method: JSON.parse(JSON.stringify(req.method))
  })
);

module.exports = router;
