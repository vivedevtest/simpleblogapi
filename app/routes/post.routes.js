var express = require('express');
var router = express.Router() ;
var Post = require('../controller/post.controller');
    
// FindAll
router.get('/', Post.findAll);

// Find a single Post by Name
router.get('/:title', Post.findByTitleName);

// Find all Products of a Author
router.get('/author/:authorId', Post.findByAuthorId);

//Save
router.post('/', Post.create);

//Update
router.put('/:titleId', Post.findByTitleIdAndUpdate);

//Delete
router.delete('/:titleId', Post.findByTitleIdAndDelete);

module.exports = router


///////////////////////////////////////////////////////////////////////////////


/* module.exports = function(app) {
 
    var Post = require('../controller/post.controller');
    
    // FindAll
    app.get('/api/posts', Post.findAll);

    // Find a single Post by Name
    app.get('/api/posts/:title', Post.findByTitleName);

    // Find all Products of a Author
    app.get('/api/posts/author/:authorId', Post.findByAuthorId);

    //Save
    app.post('/api/post', Post.save);
    
    //Update


    //Delete

    
  } */