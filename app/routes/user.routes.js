var express = require("express");
var router = express.Router();
var User = require("../controller/user.controller");
const Auth = require("../middleware/auth");

//Find All
router.get("/", User.findAll);

//Create new user
router.post("/", User.create);

//Login
router.post("/login", User.login);

//Delete
router.delete("/", Auth, User.findByUsernameAndDelete);

//Refresh Token
router.post("/refreshToken", User.refreshToken);

module.exports = router;
