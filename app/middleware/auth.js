const jwt = require("jsonwebtoken");
const User = require("../models/user.model");

const auth = async (req, res, next) => {
  try {
    if (!req.header("Authorization").includes("Bearer")) {
      return res
        .status(401)
        .json({ message: "Not authorized to access this resource" });
    }
    const token = req.header("Authorization").replace("Bearer ", "");
    const data = jwt.verify(token, `${process.env.JWT_SECRET_KEY}`);

    const user = await User.findOne({ _id: data._id }); //"tokens.token : token"

    if (!user) {
      return res.status(401).json({ message: "User not found" });
    }

    user.tokens.filter(usertoken => {
      if (usertoken.token != token) {
        return res.status(401).json({ message: "Invalid signature" });
      } else {
        req.user = user;
        req.token = token;
        next();
      }
    });
  } catch (err) {
    if (err.message.includes("expired")) {
      return res.status(403).json({ message: "Token is expired!" });
    }
    res.status(401).json({ message: "Not authorized to access this resource" });
  }
};
module.exports = auth;
