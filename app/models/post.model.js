const mongoose = require('mongoose')
const Schema = mongoose.Schema;
/* const moment = require('moment-timezone');
const dateThailand = moment.tz(Date.now(), "Asia/Bangkok"); */

const postSchema = new Schema({
  title: { type: String, trim: true, required: true, unique: true},
  description:{ type: String, trim: true, required: true},
  author : { type: Schema.Types.ObjectId, ref: 'Author' , required: true}
}, { timestamps: true, versionKey: false })

module.exports = mongoose.model('Post', postSchema)