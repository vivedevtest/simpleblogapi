const mongoose = require('mongoose')
const Schema = mongoose.Schema
/* const moment = require('moment-timezone');
const dateThailand = moment.tz(Date.now(), "Asia/Bangkok"); */

const authorSchema = new Schema({
  name: { type: String, trim: true, required: true, unique: true}
}, { timestamps: true, versionKey: false })

module.exports = mongoose.model('Author', authorSchema)