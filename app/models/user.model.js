const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    username: { type: String, trim: true, required: true, unique: true },
    password: { type: String, trim: true, required: true, minlength: 4 },
    tokens: [
      {
        token: { type: String, required: true }
      }
    ]
  },
  { timestamps: true, versionKey: false }
);

userSchema.pre("save", async function(next) {
  // Hash the password before saving the user model
  const user = this;
  if (user.isModified("password")) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

userSchema.methods.generateAuthToken = async function() {
  // Generate an auth token for the user
  const user = this;

  const token = jwt.sign({ _id: user._id }, `${process.env.JWT_SECRET_KEY}`, {
    algorithm: `${process.env.JWT_ALGORITHM}`,
    expiresIn: `${process.env.JWT_EXPIRE}`
  });

  const refreshToken = jwt.sign(
    { _id: user._id },
    `${process.env.JWT_SECRET_KEY_REFRESH}`,
    {
      algorithm: `${process.env.JWT_ALGORITHM}`,
      expiresIn: `${process.env.JWT_EXPIRE_REFRESH}`
    }
  );
  user.tokens = { token }; //user.tokens.concat({token}) //Array for multiple login (logout)
  await user.save();
  return { token, refreshToken };
};

userSchema.statics.findByCredentials = async (username, password) => {
  // Search for a user by username and password.
  const user = await User.findOne({ username });
  if (!user) {
    throw { message: "Invalid login credentials" };
  }

  const isPasswordMatch = await bcrypt.compare(password, user.password);
  if (!isPasswordMatch) {
    throw { message: "Invalid login credentials" };
  }
  return user;
};

userSchema.statics.generateAuthTokenforRefresh = async (user, refreshToken) => {
  // Generate an auth token for the user.
  const data = jwt.verify(
    refreshToken,
    `${process.env.JWT_SECRET_KEY_REFRESH}`
  );

  const token = jwt.sign({ _id: data._id }, `${process.env.JWT_SECRET_KEY}`, {
    algorithm: `${process.env.JWT_ALGORITHM}`,
    expiresIn: `${process.env.JWT_EXPIRE}`
  });

  user.tokens = { token }; //user.tokens.concat({token}) //Array for multiple login (logout)
  await user.save();
  return token;
};

const User = mongoose.model("User", userSchema);

module.exports = User;
