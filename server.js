require("dotenv").config();
require("circular-json");
const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");

//Instance
const app = express();

//Port
const PORT = `${process.env.PORT}` || 8080;

//SetType
app.use(bodyParser.json());

//Set Header
app.use((req, res, next) => {
  res.setHeader("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  res.setHeader(
    "Access-Control-Allow-Headers",
    "Content-Type, Accept, Authorization"
  );
  next();
});

//Middleware
const api = require("./app/routes/api");
app.use("/api", api);

//Route
app.all("/", (req, res) =>
  res.json({
    Message: "Welcome to Blog",
    Method: JSON.parse(JSON.stringify(req.method))
  })
);
/* require('./app/routes/author.routes')(app);
require('./app/routes/post.routes')(app); */

//Config Mongodb
mongoose.Promise = global.Promise;
const config = require("./db");

//Connect Mongodb
mongoose.connect(
  config.DBURL,
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
  },
  function(err, db) {
    if (err) {
      console.log("database is not connected");
      //db.close()
    } else {
      console.log("database is connected!!");
    }
  }
);

//Run Server
app.listen(PORT, () => {
  console.log(`Server is running on port : ${PORT}`);
});

//module.exports = app;
